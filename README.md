# Cloudwise

This project aims to test Cloudwise UI testing using Selenium and Java

## Scenario tested in this project
* List down all the employees name in each department and list out the employees having same name
* Implementing the challenges in CodeShake and asserting the results

##Prerequisites: Tools/Framework/Libraries

* Eclipse/intellij IDE
* JDK 8
* Selenium-java
* Maven
* TestNG

##Steps to execute the project:

**To run locally**: 
* Clone from repository from https://gitlab.com/priya.murugavel89/cloudwise
* Go to project root and execute the command - **mvn test**

**To view Test Reports:**
* Html reports can be found under `target/surefire-reports/emailable-report.html`
