package nl.cloudwise.assignment.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CloudWisePage {
	
	WebDriver driver;
	By whoAreWeLink = By.partialLinkText("Wie zijn wij");
	By flipBoxDiv = By.className("flip-box-front");
	By empNameHeader = By.xpath("div/h3");
	By cookieButton = By.id("CybotCookiebotDialogBodyLevelButtonAccept");
	By thisIsCloudwise = By.id("menu-item-6352");
	By allCloudwisersMenu = By.xpath("//*[@id=\"menu-item-6380\"]//a");
	
	public CloudWisePage(WebDriver driver){
		this.driver = driver;
	}
	
	public List<WebElement> getAllWhoAreWeLinks(){
		return driver.findElements(whoAreWeLink);
	}
	
	public List<WebElement> getAllFlipBoxDiv() {
		return driver.findElements(flipBoxDiv);
	}
	
	public WebElement getEmpNameHeader(WebElement element) {
		return element.findElement(empNameHeader);
	}
	
	public WebElement acceptCookies() {
		return driver.findElement(cookieButton);
	}
	
	public WebElement selectThisIsClouswiseMenu() {
		return driver.findElement(thisIsCloudwise);
	}
	
	public WebElement selectAllClouswisersSubMenu() {
		return driver.findElement(allCloudwisersMenu);
	}

}
