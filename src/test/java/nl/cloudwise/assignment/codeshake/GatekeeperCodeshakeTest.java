package nl.cloudwise.assignment.codeshake;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import nl.cloudwise.assignment.common.BaseCodeShakeTest;

public class GatekeeperCodeshakeTest extends BaseCodeShakeTest {

	@Test(priority=1)
	public void mathPositiveNumberScenario() {

		driver.findElement(By.id("mat-input-0")).clear();
		driver.findElement(By.id("mat-input-0")).sendKeys("1");
		driver.findElement(By.id("mat-input-1")).clear();
		driver.findElement(By.id("mat-input-1")).sendKeys("2");
		String mathOutput = driver.findElement(By.id("mat-input-2")).getAttribute("value");
		Assert.assertEquals(3, Integer.parseInt(mathOutput));
	}

	@Test(priority=2)
	public void mathNegativeNumberScenario() {

		driver.findElement(By.id("mat-input-0")).clear();
		driver.findElement(By.id("mat-input-0")).sendKeys("-2");
		driver.findElement(By.id("mat-input-1")).clear();
		driver.findElement(By.id("mat-input-1")).sendKeys("-2");
		String mathOutput = driver.findElement(By.id("mat-input-2")).getAttribute("value");
		Assert.assertEquals(-4, Integer.parseInt(mathOutput));

	}

	@Test(priority=3)
	public void unicornPositiveScenario() {

		driver.findElement(By.id("mat-input-3")).sendKeys("bobi");
		driver.findElement(By.id("mat-input-3")).sendKeys(Keys.ENTER);
		List<WebElement> unicornPositiveOutput = driver.findElements(By.xpath("//*[@class=\"fal fa-unicorn\"]"));
		WebElement unicornOutput = driver.findElement(By.xpath("//*[contains(text(),' Bobi Codeshake ')]"));
		Assert.assertEquals(2, unicornPositiveOutput.size());
		Assert.assertEquals("Bobi Codeshake", unicornOutput.getText());

	}

	@Test(priority=4)
	public void unicornNegativeScenario() {

		driver.findElement(By.id("mat-input-3")).clear();
		driver.findElement(By.id("mat-input-3")).sendKeys("bo");
		driver.findElement(By.id("mat-input-3")).sendKeys(Keys.ENTER);
		List<WebElement> unicornPositiveOutput = driver.findElements(By.xpath("//*[@class=\"fal fa-unicorn\"]"));
		Assert.assertEquals(1, unicornPositiveOutput.size());

	}

	@Test(priority=5)
	public void popup(){

		driver.findElement(By.className("mat-button-wrapper")).click();
		Set<String> windowIds = driver.getWindowHandles();
		Iterator<String> itr = windowIds.iterator();

		String childWindow = itr.next();
		driver.switchTo().window(childWindow);

		WebElement arena = driver.findElement(By.className("arena"));
		List<WebElement> arenaDivs = arena.findElements(By.xpath("./child::*"));
		for (int i = 0; i < arenaDivs.size(); i++) {
			WebElement divElement = arenaDivs.get(i);
			WebElement direct = divElement.findElement(By.xpath("i"));
			if (direct.getAttribute("target-for-close-action") != null) {
				direct.click();
				break;
			}
		}
	}

	@Test(priority=6)
	public void addressValidation() {

		String expectedOutput = driver
				.findElement(By.xpath(
						"//span[contains(@style,'font-style: italic')]"))
				.getText();
		driver.switchTo().frame("iframe");
		driver.findElement(By.xpath("//i[@class=\"qodef-icon-font-awesome fa fa-bars \"]")).click();
		driver.findElement(By.partialLinkText("Contact")).click();
		String actualOutput = driver
				.findElement(By
						.xpath("//div[@class=\"qodef-full-section-inner\"]//p"))
				.getText();
		Assert.assertEquals(expectedOutput, actualOutput);
		driver.switchTo().defaultContent();

	}

}
