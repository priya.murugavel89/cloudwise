package nl.cloudwise.assignment.employees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import nl.cloudwise.assignment.common.BaseTest;
import nl.cloudwise.assignment.pages.CloudWisePage;

public class GetAllEmployeeDetailsTest extends BaseTest {
	
	CloudWisePage cloudWisePage;

	@Test(priority=7)
	public void getAllEmployeeNames() {
		
		List<String> allEmployeeNames = new ArrayList<>();
		Set<String> employeeNamesWithoutDuplicates = new HashSet<>();
		Set<String> duplicatesNames = new HashSet<>();
		Actions builder = new Actions(driver);
		cloudWisePage = new CloudWisePage(driver);

		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions
				.elementToBeClickable(cloudWisePage.acceptCookies()));
		cloudWisePage.acceptCookies().click();
		cloudWisePage.selectThisIsClouswiseMenu().click();
		WebElement menu = cloudWisePage.selectThisIsClouswiseMenu();
		builder.moveToElement(menu).build().perform();
		WebDriverWait waitMenu = new WebDriverWait(driver, 10);
		waitMenu.until(ExpectedConditions.elementToBeClickable(cloudWisePage.selectAllClouswisersSubMenu()));
		WebElement menuOption = cloudWisePage.selectAllClouswisersSubMenu();
		menuOption.click();

		List<WebElement> allDepartments = cloudWisePage.getAllWhoAreWeLinks();
		
		for (int i = 0; i < allDepartments.size(); i++) {
			WebElement department = cloudWisePage.getAllWhoAreWeLinks().get(i);
			builder.moveToElement(department).build().perform();
			WebDriverWait waitWhoAreWe = new WebDriverWait(driver, 10);
			waitWhoAreWe.until(ExpectedConditions
					.elementToBeClickable(cloudWisePage.getAllWhoAreWeLinks().get(i)));
			department.click();
			allEmployeeNames.addAll(getEmployeeNamesPerDepartment());
			driver.navigate().back();
		}
		System.out.println("Listing all employees , Size = "+allEmployeeNames.size());
		for (String employeeName : allEmployeeNames) {
			System.out.println(employeeName);
			if (employeeNamesWithoutDuplicates.add(employeeName) == false) {
				duplicatesNames.add(employeeName);
			}
		}		
		System.out.println("Listing employees with same names, Size = "+duplicatesNames.size());	
		for (String duplicates : duplicatesNames) {
			System.out.println(duplicates);
		}
	}	
	private List<String> getEmployeeNamesPerDepartment() {
		
		List<String> employeeNames = new ArrayList<>();

		List<WebElement> deptEmployees = cloudWisePage.getAllFlipBoxDiv();
		for (WebElement webElement : deptEmployees) {
			WebElement employeeHeader = cloudWisePage.getEmpNameHeader(webElement);
			employeeNames.add(employeeHeader.getText());
		}

		return employeeNames;
	}
}
